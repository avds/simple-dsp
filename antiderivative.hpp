/**
 *
 */

#pragma once


#include <cstddef>
#include <cmath>


using namespace std;


namespace Sledge {
namespace DSP {


	/// Матчасть тут http://vunivere.ru/work10048
	class AntiderivativeSimple
	{
	private: //variables
		//size_t isize;  /// период интегрирования
		float XN=0;      /// накопленная сумма-первообразная
		
	public:
		
		float sum( float in[], size_t len ){
			float su=0;
			for( size_t i=0; i<len; ++i ){
				su += in[i];
			}
			return su;
		}

		//X[k] - X[N] = sum(i=N,i<k)x[i];
		void count( float in[], size_t len, float out[]=NULL ){
			if( out == NULL )
				out = in;
			for( size_t i=0; i<len; ++i ){
				out[i] = count(in[i]);
			}
		}
		
		float count( const float in ){
			float ret = XN;
			XN += in;
			return ret;
		}
		
		AntiderivativeSimple& reset(){
			XN = 0;
			return *this;
		}
	};


	/// Simplified Kalman Filter
	/// Этот интегратор спыжен с лабвью
	template< typename T=float >
	class AlphaBetaGammaFilter
	{
	/*public:
		using std::size_t;
		using std::sin;
		using std::cos;*/
	public:
		static constexpr T PI = 3.1415926535897932384626433832795;
		
		T alpha, beta=0, gamma;
		
		/// fc = 1/(2pi) used because this fc applies correct gains to frequency components >= 1Hz
		T fc = 0.159154943092;
		
		/// sample rate
		T fs;
		
		//struct { T x=0,y=0; void reset(){x=0;y=0;}; } state;
		T prev_in=0, prev_out=0;
		
	public:
		
		AlphaBetaGammaFilter( T fs, T fc=0.159154943092 ) 
			: fc(fc), fs(fs) 
		{ calculate_koefs(); };
		
		AlphaBetaGammaFilter& setCutoffFrequency( T f ){ 
			fc = f; 
			calculate_koefs(); 
			return *this; 
		};
		
		/// Calculate alpha and gamma coefs by sample rate and low-pass cutoff frequency
		void calculate_koefs()
		{
			T t1 = fc*2*PI / fs;
			gamma = cos(t1) / (sin(t1)+1);
			alpha = ((T)1 - gamma) / (T)2;
		};
		
		void reset() {};
		
		void process( T in[], size_t len, T out[]=NULL )
		{
			if(out==NULL) out=in;
			for( size_t i=0; i<len; ++i ){
				prev_out = alpha * (prev_in + in[i]) + (gamma * prev_out);
				prev_in = in[i];
				out[i] = prev_out;  // out after in because out could be == in
			}
		}
		
		T process( T in ){
			prev_in = in;
			return prev_out = alpha * (prev_in + in) + (gamma * prev_out);
		}
		
		void operator()( T in[], size_t len, T out[]=NULL ){
			process( in, len, out );
		}
		
		T operator()( T in ){
			process( in );
		}
	};


	class Antiderivative
	{
		
		
	};



} //namespace Sledge
} //namespace DSP
