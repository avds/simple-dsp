/**
 *
 */

#ifndef _SimpleDSP_hpp_
#define _SimpleDSP_hpp_


///\todo container (waveform) extends SimpleDSPContainer. Must implement length()
template<typename Container>
class SimpleDSP 
{
private:
	const Container *container_;
	
public:
		
	struct Analyzed {
		Data_t min;
		Data_t max;
		Data_t peakToPeak;
		Data_t avg;
		Data_t rms;
		Data_t middle;
	};
	
	SimpleDSP( const Container *c ){
		container_ = c;
	}
	
	/**
	 * Simply calculates average
	 */
	inline Data_t average() //const
	{
		Data_t avg = 0;
		for( Index_t i=0; i<length(); i++ )
			avg += operator[](i);
		avg /= length();
		return avg;
	}
	
	/// Iterative mean. http://www.heikohoffmann.de/htmlthesis/node134.html
	/// The same as average(), but
	/// cons: Less efficient 
	/// plus: disallow overflow
	///\note Use with huge values
	inline Data_t mean() const
	{
		Data_t avg = 0;
		for( Index_t i=0; i<length(); i++) {
			avg += ((*this)[i] - avg) / (i + 1);
		}
		return avg;
	}

	
	/**
	 * Experimental
	 */
	Data_t filterOneValueKalman( Index_t numK, const Data_t koefs[] ) const
	{
		//assert_amsg( numK < length() );
		//Data_t koefs[numK] = { 0.4, 0.3, 0.2, 0.1 };  //сумма должна быть равна 1.0
		Data_t filtered = 0;
		for( Index_t i=length()-1, j=0; 
				i>=0 && j<numK;
				i--, j++ ){
			filtered += operator[](i) * koefs[j];  //koefs[i-length()+1]
		}
		return filtered;
	}
	

	/**
	 * Xk = K*Zk + (1-K)*(X(k-1)+Uk)
	 * \param k коэффициент Калмана. Уровень доверия показаниям датчика.
	 * \param uk ожидаемое изменение (дельта)
	 */
	Data_t simpleKalman( Data_t const &k, Data_t const uk = 0 ) const
	{
		if( length() >= 2 && isnormal(operator[](-1)) && isnormal(operator[](-2)) )
			operator[](-1) = k * operator[](-1) + (1.f-k) * (operator[](-2) + uk);
		return operator[](-1);
	}
	
	/**
	 * Среднее квадратическое значение
	 */
	Data_t rms() //const
	{
		float r = 0;
		for( int i=0; i<this->length(); i++ ){
			r += (*this)[i] * (*this)[i];
		}
		r = __sqrtf( r / length() );  //should expand inline as the native VFP square root instructions. not specified in any standard
		//r = sqrt( r / length() );
		return r;
	}
	
	/**
	 * Размах сигнала
	 */
	Data_t peakToPeak() //const
	{
		float val = (*this)[0];
		float max = val, min = val;  //float max = -INFINITY, min = +INFINITY;
		for( int i=1; i<length(); i++ ){
			val = (*this)[i];
			if( max < val )
				max = val;
			if( min > val )
				min = val;
		}
		return max-min;
	}
	
	/// окресnность
	bool okrestnost( const Data_t &val, const Data_t &main, const Data_t &okr ){
		return (main-okr < val && val < main+okr);
	}
	
	/// отсев выбросов больше
	///\todo возникнет ошибка с j, когда length <=1
	///\note рассчитано на небольшое количество выбросов.
	void filter_otsev( Data_t main, Data_t okr )
	{
		for( int i=0; i<length(); i++ ){
			Data_t &val = (*this)[i];
			if( !okrestnost(val,main,okr) ){
				Index_t j = i>=1 ? i-1 : i+1;
				while( j<length() && !okrestnost((*this)[j],main,okr) ){
					j++;
				}
				val = (*this)[j];
			}
		}
	}
	
	/// тупой отсев(screening)
	void filter_otsev__( Data_t main, Data_t okr )
	{
		for( int i=0; i<length(); i++ ){
			Data_t &val = (*this)[i];
			if( val < -okr )
				val=-okr;
			else if( val > okr )
				val=okr;
		}
	}

	
	/**
	 * Полный анализ, чтоб не прогонять массив по нескольку раз. Эффективнее, если нужно получить несколько параметров
	 * const-сслыка на стэк допускается http://herbsutter.com/2008/01/01/gotw-88-a-candidate-for-the-most-important-const/
	 * однако похоже ARM CC (Keil) об этом не знает
	 */
	const Analyzed& analyze() /*const*/
	{
		Analyzed ret;  // const reference is to suppress warnings
		Data_t val = (*this)[0];
		ret.max = val; ret.min = val;
		ret.avg = val; ret.rms = val*val;
		for( int i=1; i<length(); i++ ){
			val = (*this)[i];
			if( ret.max < val )
				ret.max = val;
			if( ret.min > val )
				ret.min = val;
			ret.avg += val;
			ret.rms += val * val;
		}
		ret.peakToPeak = ret.max - ret.min;
		ret.avg /= length();
		ret.rms = sqrt( ret.rms / length() );
		ret.middle = ret.min + ret.peakToPeak /2;
		return ret;  // We may asiign stack based ref only to const ref!
	}
	
	/**
	 * 
	 */
	const Analyzed* analyze(Analyzed *an) /*const*/
	{
		//if( isEmpty ) {*an = {NAN}; return an}
		Data_t val = (*this)[0];
		an->max = val; an->min = val;
		an->avg = val; an->rms = val*val;
		for( int i=1; i<length(); i++ ){
			val = (*this)[i];
			if( an->max < val )
				an->max = val;
			if( an->min > val )
				an->min = val;
			an->avg += val;
			an->rms += val * val;
		}
		an->peakToPeak = an->max - an->min;
		an->avg /= length();
		an->rms = sqrt( an->rms / length() );
		an->middle = an->min + an->peakToPeak /2;
		return an;  // We may asiign stack based ref only to const ref!
	}
	
}

#endif //_SimpleDSP_hpp_